module.exports = (() => {
    return {
        "profiles": [
            {
                "name": "Bible Deck",
                "scope": "liveVerse",
                "styles": [
                    {
                        "name": "Colors",
                        "properties": "bibleDeck.configs.colors.json"
                    }, {
                        "name": "Fonts",
                        "properties": "bibleDeck.configs.fonts.json"
                    }, {
                        "name": "Layout",
                        "properties": "bibleDeck.configs.layout.json"
                    }, {
                        "name": "Tables",
                        "properties": "bibleDeck.configs.tables.json"
                    }, {
                        "name": "Transitions",
                        "properties": "bibleDeck.configs.transitions.json"
                    }, {
                        "name": "Type",
                        "properties": "bibleDeck.configs.type.json"
                    }, {
                        "name": "Forms",
                        "properties": "bibleDeck.configs.form.json"
                    }, {
                        "name": "Forms 2",
                        "properties": "bibleDeck.configs.form1.json"
                    }, {
                        "name": "Forms 3",
                        "properties": "bibleDeck.configs.form2.json"
                    }
                ]
            }
        ]
    }
})();