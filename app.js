// ~~~~~~~~~~~~~~~~~
// ### VARIABLES ###
const pug = require('pug');

// settings
const ENV = process.env.NODE_ENV || "dev";
const port = 8080;
let host = "http://localhost:" + port + "/";

// const { Socket } = require("dgram");
// express
const express = require("express");
const app = express();
const path = require("path");

const bibleDeck = require("./data/app.config");
bibleDeck.styles = require("./services/SCSS.service")({"profiles": bibleDeck.profiles});
bibleDeck.bible = require("./data/NKJV.json");
app.set("bibleDeck", bibleDeck);

// socket.io
const server = require('http').createServer(app);
const io = require('socket.io')(server);

// ~~~~~~~~~~~~~~~~~~~~~
// ### CONFIGURATION ###

// ### BODY PARSER ### //
app.use(express.json());
app.use(express.urlencoded({ extended: true }));


// ### PUBLIC DIRECTORY ### //
app.use(express.static(path.join(__dirname,"/_public")));


// ### ROUTES ### //
// -- index
let indexRoute = require(path.join(__dirname,"/routes/deck.route"));
app.use("/", indexRoute);

// -- api
let apiRoute = require(path.join(__dirname,"/routes/api.route"));
app.use("/api", apiRoute);

// -- stream viewer
let streamRoute = require(path.join(__dirname,"/routes/stream.route"));
app.use("/stream", streamRoute);

// -- live viewer
let liveRoute = require(path.join(__dirname,"/routes/live.route"));
app.use("/live", liveRoute);

// -- refresh viewer
let refreshRoute = require(path.join(__dirname,"/routes/refresh.route"));
app.use("/refresh", refreshRoute);

// -- css
let cssRoute = require(path.join(__dirname,"/routes/css.route"));
app.use("/css", cssRoute);



// ### TEMPLATE ENGINE ### //
app.set("views", path.join(__dirname,"/views"));
app.set("view engine", "pug");


// ### SERVER ERROR HANDLER ### //
app.use((err, req, res, next) => {
    let _o = {
        data: "You done messed up...",
        img: "https://media.giphy.com/media/Rkis28kMJd1aE/giphy.gif",
        msg: err
    }
    res.status(500);
    res.render('error', _o);
});



// ### INITIATE SERVER ### //
io.on('connection', socket => {
    console.log("Client", socket.id, "has connected");

    socket.on("verse", (val)=>{
        let output = {
            "html": pug.renderFile(path.join(__dirname,"/views/live.partial.pug"), val)
        };
        socket.broadcast.emit("verse", output);
    })

    socket.on("clear", ()=>{
        socket.broadcast.emit("clear");
    })

    socket.on('disconnect', () => {
        console.log(socket.id, "disconnected");
    });
});

server.listen(port, () => {
    if(ENV == "dev") console.log("Server running at:", host);
});
