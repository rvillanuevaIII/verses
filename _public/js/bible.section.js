(($SUPER)=>{
    // ########################## //
	// ### PRIVATE PROPERTIES ### //
	// ########################## //
	var rootClass = "s-bible";
	var typePrefix = rootClass.charAt(0);
	var objectName = rootClass.replace(typePrefix + "-", "");

	let clearBtn = document.getElementById("clearBtn");
	let prevBtn = document.getElementById("prevBtn");
	let nextBtn = document.getElementById("nextBtn");
	let _target, activeObj, isClear, bookChapter;

    // ################### //
	// ### CONSTRUCTOR ### //
	// ################### //
	function Init() {
		_target = document.getElementsByClassName(rootClass)[0];
		if(!isClear) clearBtn.disabled = true;
	}

	// -- PUBLIC METHODS -- //
	Init.prototype.setVerses = function(_verses) {
		bookChapter = _verses;
	}
	Init.prototype.broadcastVerse = function(elm) {
		if(activeObj) activeObj.element.classList.remove("u-active");
		activeObj = {
			"element": elm,
			"index": parseInt(elm.dataset.index, 10)
		}
		activeObj.element.classList.add("u-active");
		activeObj.element.scrollIntoView({
            behavior: 'smooth'
        });
		clearBtn.disabled = false;

		let prevElement = activeObj.element.previousElementSibling;
		if(!prevElement) prevBtn.disabled = true;
		else prevBtn.disabled = false;

		let nextElement = activeObj.element.nextElementSibling;
		if(!nextElement) nextBtn.disabled = true;
		else nextBtn.disabled = false;

		let output = {
			"book": bookChapter.book,
			"chapter": bookChapter.chapter,
		}
		Object.assign(output, bookChapter.verses[activeObj.index]);
		socket.emit("verse", output);
	}
    Init.prototype.goHome = function() {
        _target.classList.remove(rootClass+"--active");
		this.clearAll();
    }
    Init.prototype.nextVerse = function() {
		let nextElement = activeObj.element.nextElementSibling;
		if(nextElement) {
			activeObj.element.classList.remove("u-active");
			activeObj.element = nextElement;
			this.broadcastVerse(activeObj.element);
		}
	}
    Init.prototype.previousVerse = function() {
		let nextElement = activeObj.element.previousElementSibling;
		if(nextElement) {
			activeObj.element.classList.remove("u-active");
			activeObj.element = nextElement;
			this.broadcastVerse(activeObj.element);
		}
	}
    Init.prototype.clearAll = function() {
		activeObj.element.classList.remove("u-active");
		activeObj = null;
		isClear = false;
		clearBtn.disabled = true;
		prevBtn.disabled = true;
		nextBtn.disabled = true;
		socket.emit("clear");
	}
    
    // -- initiate into Rexus object
	$SUPER[objectName] = new Init();
})(BibleDeck);