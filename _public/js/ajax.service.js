
// ### AJAX HANDLER ### //
(($SUPER) => {
    
    /*
        Known possible limitations of this script is
        that this may only handle one request at a time.
        Therefore, if any request is not yet finished,
        all actions associated with the first request
        may never fire.
    */

    // ### PRIVATE PROPERTIES ### //
    var fnQueue;

    // ### CONSTRUCTOR ### //
    function Init(options, fn) {
        /*
            DATA SAMPLE
            {
                url: [[STRING]],
                method: [[POST_METHOD]],
                data: [[OBJECT]]
            }
        */
        var xReq = new XMLHttpRequest();
        var xReqMethod = (options.method) ? options.method : "GET";
        xReq.addEventListener("progress", ajaxProgress);
        xReq.addEventListener("load", ajaxComplete);
        xReq.addEventListener("error", ajaxFailed);
        xReq.addEventListener("abort", ajaxCanceled);
        xReq.open(xReqMethod, options.url);
        xReq.setRequestHeader("Content-Type", "application/json;charset=UTF-8");

        if (options.method == "POST") xReq.send(JSON.stringify(options.data));
        else xReq.send();
    
        fnQueue = fn;
    }
    
    // --- PRIVATE METHODS --- //
    function ajaxProgress (oEvent) {
        if (oEvent.lengthComputable) {
            var percentComplete = oEvent.loaded / oEvent.total * 100;
            // ...
        } else {
            // Unable to compute progress information since the total size is unknown
        }
    }    
    function ajaxComplete() {
        fnQueue(this.response);
    }
    function ajaxFailed() {
        console.log('error');
    }
    function ajaxCanceled() {
        console.log("The transfer has been canceled by the user.");
    }

    $SUPER["ajax"] = Init;
})(BibleDeck);