(($SUPER) => {

	// ########################## //
	// ### PRIVATE PROPERTIES ### //
	// ########################## //

	var rootClass = "e-bibleLink";
	var typePrefix = rootClass.charAt(0);
	var objectName = rootClass.replace(typePrefix + "-", "");
	let _target;

	// -- Verify if manager exists
	if(!$SUPER) {
		console.error("BibleDeck Manager needs to be preloaded for the \""+rootClass+"\" to work properly");
		return;
	}

	// ################### //
	// ### CONSTRUCTOR ### //
	// ################### //
	function Init() {
		let components = document.getElementsByClassName(rootClass);
        let a = components.length-1;
        let component;

        for(a; a>=0; a--) {
            component = components[a];
            component.addEventListener("click", btnAction);
        }
	}

	// ####################### //
	// ### PRIVATE METHODS ### //
    // ####################### //
    function btnAction() {
		_target = document.getElementById(this.dataset.target);
		_target.classList.add("u-loading");

		BibleDeck.ajax({
			url: "api"+this.dataset.api
		}, (resp) => {
			//- set views/verses
			let view = _target.getElementsByClassName("s-bible__verses__view")[0];
			resp = JSON.parse(resp);
			view.innerHTML = resp.rendered;
			$SUPER.bible.setVerses(resp.raw);

			//- setup classes
			_target.classList.remove("u-loading");
			_target.classList.add("s-bible--active");
		})
    }
    
	// -- initiate into Rexus object
	$SUPER[objectName] = new Init();
})(BibleDeck);