const express = require("express");
const router = express.Router();
const pug = require("pug");

let BibleDeck;

router.route("/:book/:chapter")
    .get((req, res) => {
        let _book = req.params.book;
        let _chapter = req.params.chapter;
        BibleDeck = (BibleDeck) ? BibleDeck : req.app.get("bibleDeck");

        let rawData = BibleDeck.bible.books[_book].chapters[_chapter];
        rawData.book = BibleDeck.bible.books[_book].name;
        rawData.chapter = parseInt(_chapter, 10) + 1;
        
        let renderedHTML = pug.renderFile("./views/verses.pug", rawData);
        
        res.json({
            "raw": rawData,
            "rendered": renderedHTML
        });
    })

module.exports = router;