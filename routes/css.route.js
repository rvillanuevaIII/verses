const router = require("express").Router();
var fs = require("fs");
var BibleDeck;


router.route("/:scope")
    .get((req, res) => {
        let cssScope = req.params.scope;
        BibleDeck = (BibleDeck) ? BibleDeck : req.app.get("bibleDeck");
        stylesheet = BibleDeck.styles[cssScope];
        if(!stylesheet) res.status(404).send("Invalid CSS Route");
        else {
            res.set("Content-Type", "text/css");
            res.send(stylesheet.css);
        }
    });

module.exports = router;