const express = require("express");
const router = express.Router();

let BibleDeck;
function sortNames(a, b) {
    var nameA = a.name.toUpperCase(); // ignore upper and lowercase
    var nameB = b.name.toUpperCase(); // ignore upper and lowercase
    if (nameA < nameB) return -1;
    if (nameA > nameB) return 1;
    // names must be equal
    return 0;
}

router.route("/")
    .get((req, res) => {
        BibleDeck = (BibleDeck) ? BibleDeck : req.app.get("bibleDeck");
        let books = BibleDeck.bible.books.map((obj,index) => {
            return {"index": index, "name": obj.name, "chapters": obj.chapters.length};
        });

        res.render("deck", {
            "scripts": [
                "ajax.service.js",
                "smoothScroll.service.js",
                "bible.section.js",
                "bibleLink.element.js"
            ],
            "books": books.sort(sortNames)
        });
    })

module.exports = router;