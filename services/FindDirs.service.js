const { lstatSync, readdirSync } = require('fs');
const { join } = require('path');
const isDirectory = source => lstatSync(source).isDirectory()
const getDirectories = source =>
  readdirSync(source).map(name => join(source, name)).filter(isDirectory)

// This module will only return the first instance of a directory
module.exports = ((patt, path, listed = false) => {
  let srcPath = getDirectories(path);
  let list = (listed) ? [] : false;
  let inc = 0;
  let a = 0;    
  let _dir, isValid;
  for(a = (srcPath.length - 1); a >= 0; a--) {
      _dir = srcPath[inc];
      isValid = patt.test(_dir);
      if(listed) {
        if(isValid) list.push(_dir);
        else continue;
      } else {
        if(isValid) {
          list = _dir;
          break;
        }
      }
      inc++;
  }
  return list;
});