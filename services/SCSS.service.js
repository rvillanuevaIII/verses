require("../services/String.service");
const sass = require("node-sass");
const globFiles = require("../services/Glob.service");
const findDirs = require("../services/FindDirs.service");
const { lstatSync } = require('fs');
const isDirectory = source => lstatSync(source).isDirectory()


// -- convert the json formatted variables into readable sass variables
function convertVariables(obj) {
    let a = obj.configs.length - 1;
    let sassVars = "";
    let b, configFile, sassObj, sassKey, sassVal;

    for(a; a>=0; a--) {
        configFile = obj.configs[a].properties;
        sassObj = Object.entries(require("../"+obj.path+configFile));
        b = sassObj.length - 1;

        for(b; b>=0; b--) {
            [sassKey,sassVal] = sassObj[b];
            sassVars = "$" + sassKey + ":" + sassVal + ";" + sassVars;
        }
    }
    return sassVars;
}

module.exports = ((opt) => {

    const SassStreams = function() {

        let _output = {};        
        let scopeLog = {}; // helps to keep record of scanned SCSS scope directories
        let a = b = c = 0;
        let scope, profile;
        let scssDirectories, scssDirectory, scssThemes;
        let scssFiles, scssFile;
        let scssVars, scssMain;

        // -- validations
        if(!opt.rootPath) opt.rootPath = "./styles/";
        if(!opt.profiles) return;

        //- profile groups
        for(a=(opt.profiles.length-1); a>=0; a--) {
            profile = opt.profiles[a];
            profileID = profile.name.toCamelCase();
            scope = (isDirectory(opt.rootPath+profile.scope)) ? (opt.rootPath+profile.scope) : false;
            scssMain = "";
            configMain = "@import ";

            /*
                Check if a scope directory has been scanned to be reused
                or create a new key in the scopeLog
            */
            if(!scopeLog[profile.scope]) {
                scssDirectories = findDirs(/\d_.+/, scope, true);
                scopeLog[profile.scope] = scssDirectories;
            } else scssDirectories = scopeLog[profile.scope];
            
            /*
                Generate an import string in SASS format based on the
                files within the scope directory
            */
            for(b=(scssDirectories.length-1); b>=0; b--) {
                scssDirectory = scssDirectories[b];
                scssSplit = scssDirectory.split("/").pop();
                
                scssThemes = /themes/.test(scssDirectory); // returns true if theme is in the filename
                scssConfigs = /configs/.test(scssSplit); // returns true if "config" is in the filename

                if (scssThemes) scssGlob = scssDirectory + "/" + profileID + "/**/*.scss";
                else if (scssConfigs) {
                    scssVars = convertVariables({
                        "profile": profileID,
                        "path": scssDirectory + "/" + profileID + "/",
                        "dest": opt.distPath,
                        "subDir": scssSplit,
                        "configs": profile.styles
                    });
                    continue;
                } else scssGlob = scssDirectory + "/**/*.scss"

                scssFiles = globFiles(scssGlob);
                for(c=(scssFiles.length-1); c>=0; c--) {
                    scssFile = scssFiles[c];
                    scssMain = "'" + scssFile.path + "'," + scssMain;
                }
            }
            
            // -- apply missing syntax and merge variables with declarations
            scssMain = "@import" + scssMain; // replace first comma with "@import"
            scssMain = scssMain.trimEnd().slice(0,-1) + ";"; // adds ";" at the end of string
            scssMain = scssVars + scssMain;

            // -- Finalize SASS file and compile
            _output[profileID] = sass.renderSync({
                data: scssMain,
                outputStyle: "compressed"
            });
        }
        // outputCSS(_output);
        return _output;
    }

    return new SassStreams;
});