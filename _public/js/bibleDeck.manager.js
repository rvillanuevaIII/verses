(function() {

	// ### PREVALIDATION ### //
	if(this.BibleDeck) return false;

	// ### PRIVATE PROPERTIES ### //
	let tagBank = [];

	// ### CONSTRUCTOR ### //
	function Init() {};

	// ### PRIVATE METHODS ### //
	function verifyHash(hash) {
		return (tagBank.indexOf(hash) >= 0);
	};
	function generateHash() {
		return Math.random().toString(36).substring(2, 8);
	};

	// ### PUBLIC METHODS ### //
	Init.prototype.tag = function(prefix) {		

		// -- prevalidate "prefix"
		if (!prefix) prefix = "";
		else prefix = prefix + "-";
		
		// -- generate new hash
		let newHash = prefix + generateHash();
		
		// -- verify that hash doesn't exist in bank
		while (verifyHash(newHash)) newHash = prefix + generateHash();

		// -- push unique hash to bank
		tagBank.push(newHash);

		// -- return unique hash
		return newHash;
	}
	
	// -- initiator
	this.BibleDeck = new Init();
})();